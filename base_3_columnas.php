<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Alex Campusano">
        <title>itoRank - Login</title>
        <!-- Font awesome -->
        <script src="https://kit.fontawesome.com/3730a38f20.js" crossorigin="anonymous"></script>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="css/master.css">
        
    </head>
    <body class="home">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-danger">
            <a class="navbar-brand" href="#">
            <img src="img/logo_itorank_b.svg" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <form class="form-inline ml-auto">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                    <!-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->
                </form>
                <ul class="navbar-nav  ml-auto">
                    <li class="nav-item px-1">
                        <a class="nav-link" href="#">
                            <i class="fas fa-th"></i>
                            <span class="sr-only">Más Aplicaciones</span>
                        </a>
                    </li>
                    <li class="nav-item px-1">
                        <a class="nav-link" href="#">
                            <i class="fas fa-question-circle"></i>
                            <span class="sr-only">Ayuda</span>
                        </a>
                    </li>
                    <li class="nav-item px-1">
                        <a class="nav-link" href="#">
                        <i class="fas fa-bell"></i>
                            <span class="sr-only">Notificaciones</span>
                        </a>
                    </li>
                    <li class="nav-item px-1 ">
                        <a class="nav-link" href="#">
                            <i class="fas fa-ellipsis-v"></i>
                            <span class="sr-only">Opciones</span>
                        </a>
                    </li>
                    <li class="nav-item pl-4">
                        <a href="#" class="usuario_top">
                            <img src="img/user.jpg" alt="" class="rounded-circle">
                        </a>
                    </li>
                </ul>
                
            </div>
        </nav>

        <main role="main" class="container">
            <div class="row home mt-5">
                <section class="central col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
                    
                    <div class="card mb-3 px-2 py-1">Comprte</div>

                    <div class="card mb-3 px-2 py-1">
                        
                    </div>

                </section>
                <aside class="aside_izq col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-6 col-12">
                    <div class="card mb-3 px-2 py-1 info_personal">Información Personal</div>
                    <div class="card mb-3 px-2 py-1 keywords">Keywords</div>
                    <div class="card mb-3 px-2 py-1 performance">Performance</div>
                    <div class="card mb-3 px-2 py-1 experiencia">Experiencia</div>
                    <div class="card mb-3 px-2 py-1 conocimientos">Conocimientos</div>
                </aside>
                <aside class="aside_der col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card mb-3 px-2 py-1 mas_profesionales">Más Profesionales</div>
                    <div class="card mb-3 px-2 py-1 invitacion">Invita a mas personas</div>
                    <div class="card mb-3 px-2 py-1 extra_lateral">Extra Lateral</div>
                </aside>
            </div>
        </main>
        <!-- Bootstrap JS  -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous">
        </script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>

    </body>
</html>
