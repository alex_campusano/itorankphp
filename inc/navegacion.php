<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-danger">
            <a class="navbar-brand" href="#">
            <img src="img/logo_itorank_b.svg" alt="itorank">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <form class="form-inline ml-auto mt-sm-0 mt-4 search_nav">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                    <!-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->
                </form>
                <ul class="navbar-nav  ml-auto">
                    <li class="nav-item px-1">
                        <a class="nav-link" href="#">
                            <i class="fas fa-th"></i>
                            <span class="sr-only">Más Aplicaciones</span>
                        </a>
                    </li>
                    <li class="nav-item px-1">
                        <a class="nav-link" href="#">
                            <i class="fas fa-question-circle"></i>
                            <span class="sr-only">Ayuda</span>
                        </a>
                    </li>
                    <li class="nav-item px-1">
                        <a class="nav-link" href="#">
                        <i class="fas fa-bell"></i>
                            <span class="sr-only">Notificaciones</span>
                        </a>
                    </li>
                    <li class="nav-item px-1 dropdown">
                        <a class="nav-link" href="#" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                            <span class="sr-only">Opciones</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right p-3" aria-labelledby="dLabel">
                            <ul>
                                <li>hola</li>
                                <li>Buenos</li>
                                <li>Días</li>
                            </ul>
                        </div>
                    </li>
                    
                    <li class="nav-item pl-4">
                        <a href="#" class="usuario_top">
                            <img src="img/user.jpg" alt="" class="rounded-circle">
                        </a>
                    </li>
                    
                </ul>
            </div>
        </nav>