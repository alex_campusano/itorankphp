                    <div class="card comparte_top mb-3">
                        <div class="card-header py-2 px-3">
                            <span class="titulo_card mr-auto">
                                Comparte con la comunidad
                            </span>
                            <span class="ubicacion ml-auto">
                                <i class="fas fa-map-marker-alt"></i> Las Condes, RM.
                            </span>
                        </div>
                        <div class="card-body px-2 py-1">
                            <input type="text" name="publicacion_basica" class="publicacion_basica" placeholder="Escribe aquí tu estado o selecciona un tipo de publicación ">
                        </div>
                        <div class="card-footer">
                            <a href="#" class="btn_tipo_publicacion mr-3" data-toggle="modal" data-target="#exampleModal">
                                <i class="fas fa-skull-crossbones mr-2"></i>
                                Nuevo Peligro
                            </a>
                            <a href="#" class="btn_tipo_publicacion mr-3" data-toggle="modal" data-target="#exampleModal">
                                <i class="fas fa-scroll mr-2"></i>
                                Nueva Norma
                            </a>
                            <a href="#" class="btn_tipo_publicacion mr-3" data-toggle="modal" data-target="#exampleModal">
                                <i class="fas fa-tag mr-2"></i>
                                Nuevo Precio
                            </a>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog  modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Tipo de Publicación</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Formulario <br> <br> <br> <br> <br> <br>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Pubicar</button>
                                </div>
                            </div>
                        </div>
                    </div>