                    <div class="card publicacion_full mb-3">
                        <div class="card-header p-2">
                            <div class="usuario_publicante">
                                <img src="img/user_2.jpg" alt="" class="rounded-circle up_imagen mr-2">
                                <div class="datos_up">
                                    <div class="up_nombre">Nombre del usuario</div>
                                    <div class="up_tiempo_publicacion">23 de Marzo, 10:21 PM</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="imagen_publicacion">
                                <img src="img/ejemplo_1.jpg" alt="Titulo de la publicacion" class="imagen_p">
                            </div>
                            <div class="detalle_publicacion p-3">
                                <p><a href="#" class="badge badge-pill badge-primary">Tipo de publicacion</a></p>
                                <div class="titulo_publicacion mt-2">
                                    Inició el proyecto Escuela A56 de Cachilluyo
                                </div>
                                <div class="texto_publicacion mt-2">
                                    Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
                                    Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
                                </div>
                            </div>
                            <div class="acciones_publicacion p-3">
                                <div class="accion_me_gusta mr-5">
                                    <a href="#">
                                        <i class="fas fa-thumbs-up"></i> Me gusta
                                    </a>
                                </div>
                                <div class="accion_comparte">
                                    <a href="#">
                                        <i class="fas fa-share"></i> Compartir
                                    </a>
                                </div>
                            </div>
                            <div class="comentarios_publicaciones p-3">
                                <div class="comentario_cp pb-2 mt-2">
                                    <div class="imagen_uc mr-3 mb-2">
                                        <img src="img/user_1.jpg" alt="" class="rounded-circle mr-2">
                                        <div class="nombre_tc">Sofia Campusano</div>
                                    </div>
                                    <div class="texto_comentario p-2 mr-2 mb-2">
                                        <div class="comentario_tc">Lorem ipsum dolor sit amet consectetur adipisicing elit?</div>
                                    </div>
                                    <div class="icono_tc">
                                        <i class="fas fa-thumbs-up"></i>
                                    </div>
                                </div>
                                <div class="comentario_cp pb-2 mt-2">
                                    <div class="imagen_uc mr-3 mb-2">
                                        <img src="img/user_1.jpg" alt="" class="rounded-circle mr-2">
                                        <div class="nombre_tc">Sofia Campusano</div>
                                    </div>
                                    <div class="texto_comentario p-2 mr-2 mb-2">
                                        <div class="comentario_tc">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</div>
                                    </div>
                                    <div class="icono_tc">
                                        <i class="fas fa-thumbs-up"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="usuario_comentario">
                                <div class="imagen_uc mr-3">
                                    <img src="img/user.jpg" alt="" class="rounded-circle">
                                </div>
                                <input class="comentario_uc py-1 px-2" type="text" name="comentario_p123456" placeholder="Agrega un comentario" >
                            </div>
                        </div>                        
                    </div>