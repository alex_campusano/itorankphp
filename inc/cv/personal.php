                            <article id="cv_personal" class="p-4">
                                <header class="t16_rojo" data-toggle="collapse" href="#personal_contenido" role="button" aria-expanded="false" aria-controls="personal_contenido">Personal</header>
                                <main class="collapse show" id="personal_contenido">
                                    <div class="editar_imagen_usuario mt-3">
                                        <div class="label_12_ttu_fwb_gris">Imagen de perfil (avatar)</div>
                                        <img src="img/user.jpg" alt="" class="rounded-circle imagen_usuario" >
                                    </div>
                                    <div class="editar_datos_usuario">
                                        <form class="mt-4">
                                            <div class="form-group">
                                                <label for="nombre_completo" class="label_12_ttu_fwb_gris">Nombre Completo</label>
                                                <input type="text" class="form-control" id="nombre_completo" placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label for="nombre_usuario" class="label_12_ttu_fwb_gris">Nombre de Usuario</label>
                                                <input type="text" class="form-control" id="nombre_usuario" placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label for="profesion" class="label_12_ttu_fwb_gris">Profesion</label>
                                                <input type="text" class="form-control" id="profesion" placeholder="Ej. Arquitecto">
                                            </div>
                                            <div class="form-group">
                                                <label for="fecha_nacimiento" class="label_12_ttu_fwb_gris">Fecha de Nacimiento</label>
                                                <input type="date" class="form-control" id="fecha_nacimiento" placeholder="Ej. Arquitecto">
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="inputEmail4" class="label_12_ttu_fwb_gris">Email</label>
                                                    <input type="email" class="form-control" id="inputEmail4">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="telefono_usuario" class="label_12_ttu_fwb_gris">Teléfono</label>
                                                    <input type="tel" class="form-control" id="telefono_usuario">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="resumen" class="label_12_ttu_fwb_gris">Resumen</label>
                                                <textarea type="text" class="form-control" id="resumen" placeholder="Resumen"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                </main>
                            </article>