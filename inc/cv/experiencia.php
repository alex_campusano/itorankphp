                            <article id="cv_experiencia" class="p-4">
                                <header class="t16_rojo" data-toggle="collapse" href="#experiencia_cv_contenido" role="button" aria-expanded="false" aria-controls="experiencia_cv_contenido">Experiencia</header>
                                <main class="collapse pt-4" id="experiencia_cv_contenido">
                                <div class="row experiencia mt-3 mb-5">
                                        <div class="col-2">
                                            <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                        </div>
                                        <div class="col-10">
                                            <div class="cargo_trabajo">Cargo desempeñado</div>
                                            <span class="lugar_info_cv">Lugar de trabajo</span>
                                            <span class="fecha_info_cv">fecha de trabajo</span>
                                            <span class="direccion_info_cv">Direccion</spanp>
                                            <div class="label_13_ttu_fwb_rojo mb-2">Keywords</div>
                                            <div class="keywords_relacionados">
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Jefe de proyectos</a>
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura de Proyectos</a>
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Gestión de proyectos</a>
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Sistemas</a>
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row experiencia mt-3 mb-5">
                                        <div class="col-2">
                                            <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                        </div>
                                        <div class="col-10">
                                            <div class="cargo_trabajo">Cargo desempeñado</div>
                                            <span class="lugar_info_cv">Lugar de trabajo</span>
                                            <span class="fecha_info_cv">fecha de trabajo</span>
                                            <span class="direccion_info_cv">Direccion</spanp>
                                            <div class="label_13_ttu_fwb_rojo mb-2">Keywords</div>
                                            <div class="keywords_relacionados">
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Jefe de proyectos</a>
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura de Proyectos</a>
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Gestión de proyectos</a>
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Sistemas</a>
                                                <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal_experiencia">Agregar experiencia laboral</button>
                                    
                                    
                                    
                                    <div class="modal fade" id="modal_experiencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-dialog-centered modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Experiencia Laboral</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    
                                                    Pronto



                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                                                    <button type="button" class="btn btn-primary">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </main>
                            </article>