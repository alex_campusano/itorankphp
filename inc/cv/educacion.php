                            <article id="cv_educacion" class="p-4">
                                <header class="t16_rojo" data-toggle="collapse" href="#educacion_cv_contenido" role="button" aria-expanded="false" aria-controls="educacion_cv_contenido">Educación</header>
                                <main class="collapse pt-4" id="educacion_cv_contenido">
                                   <div class="row educacion mt-3 mb-5">
                                        <div class="col-2">
                                            <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_educacion">
                                        </div>
                                        <div class="col-10">
                                            <div class="titulo_educacion">Ingeniaería en algo</div>
                                            <span class="lugar_info_cv">Lugar de estudio</span>
                                            <span class="fecha_info_cv">fecha de estudio</span>
                                            <span class="escuela_educacion_cv">Escuela </span>
                                            <span class="direccion_info_cv">Direccion</span>
                                        </div>
                                    </div>
                                    
                                    <div class="row experiencia mt-3 mb-5">
                                        <div class="col-2">
                                            <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                        </div>
                                        <div class="col-10">
                                            <div class="titulo_educacion">Ingeniaería en algo</div>
                                            <span class="lugar_info_cv">Lugar de estudio</span>
                                            <span class="fecha_info_cv">fecha de estudio</span>
                                            <span class="escuela_educacion_cv">Escuela </span>
                                            <span class="direccion_info_cv">Direccion</span>
                                        </div>
                                    </div>
                                    
                                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal_educacion">Agregar Estudios</button>
                                    
                                    
                                    
                                    <div class="modal fade" id="modal_educacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-dialog-centered modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Estudios</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    

                                                    <?php include('steps.php') ?>
                                                    

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                                                    <button type="button" class="btn btn-primary">Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </main>
                            </article>