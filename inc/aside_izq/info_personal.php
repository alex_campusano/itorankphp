                    <div class="card mb-3 px-2 py-1 info_personal">
                        <div class="usuario_info mb-2">
                            <img src="img/user.jpg" alt="Nombre de usuario" class="rounded-circle">
                            <div class="nombre_info_perso mt-2">Scarlett Nayareth <br> Johansson Pérez</div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <div class="creado_bd mt-2">
                            <div class="label_10_ttu">Creado</div>
                            <div class="info_aside mb-2">26 febrero 2021</div>
                            <div class="label_10_ttu">Cumpleaños</div>
                            <div class="info_aside mb-2">22 noviembre 1984</div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <div class="ultima_actualizacion">
                            <div class="label_10_ttu">última Actualizacion</div>
                            <div class="info_aside mb-2">24 marzo 2021</div>
                        </div>
                    </div>