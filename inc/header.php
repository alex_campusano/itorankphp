<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Alex Campusano">
        <title>itoRank</title>
        <!-- Font awesome -->
        <script src="https://kit.fontawesome.com/3730a38f20.js" crossorigin="anonymous"></script>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- stepper  -->
        <link rel="stylesheet" href="css/stepper.jquery.css">
        <link href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1581152092/smartwizard/smart_wizard.min.css" rel="stylesheet" type="text/css" />
        <!-- <link href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1581152092/smartwizard/smart_wizard_theme_dots.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="css/master.css">
        
    </head>