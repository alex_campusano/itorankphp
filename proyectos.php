<?php include_once('inc/header.php'); ?>
    <body class="home">
    <?php include_once('inc/navegacion.php'); ?>
        <main role="main" class="container">
            <div class="row home mt-2">
                <section class="central col col-xl-7 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">

                    <?php include('inc/proyectos_base.php'); ?>

                </section>
                <aside class="aside_izq col col-xl-2 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-6 col-12">
                    
                    <?php include_once('inc/aside_izq/info_personal.php') ?>

                    <?php include_once('inc/aside_izq/keywords.php') ?>

                    <?php include_once('inc/aside_izq/performance.php') ?>

                    <?php include_once('inc/aside_izq/experiencia.php') ?>

                    <?php include_once('inc/aside_izq/conocimientos.php') ?>
                    
                </aside>
                <aside class="aside_der col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-6 col-12">
                    
                    <?php include_once('inc/aside_der/mas_profesionales.php') ?>
                    
                    <?php include_once('inc/aside_der/invitar.php') ?>
                    
                </aside>
            </div>
        </main>
        <!-- Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous">
        </script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
    </body>
</html>
