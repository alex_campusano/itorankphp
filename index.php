<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Alex Campusano">
        <title>itoRank - Login</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/master.css">
        <!-- Custom styles for this template -->
    </head>
    <body class="login">
        <nav class="navbar navbar-expand-md fixed-top navbar_login">
            <a class="navbar-brand" href="#">
                <img src="img/logo_itorank.svg" alt="">
            </a>
            <div class="ml-auto">
                <a href="#">Inicia sesión</a>
            </div>
        </nav>

        <main role="main" class="container">
            <div class="row login_form mt-5">
                <div class="col-sm-5 offset-sm-1">
                    <h2>¡Te damos la bienvenida a tu comunidad profesional!</h2>
                    <h4 class="mt-5">ingresa</h4>
                    <form  class="needs-validation mt-5" novalidate>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Nombre de usuario o correo electrónico" id="exampleInputEmail1" aria-describedby="emailHelp" required >
                            <div class="invalid-feedback">
                                <small id="emailHelp" class="form-text text-danger">Tenemos problemas con tu correo electrónico.</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña" required>
                            <div class="invalid-feedback">
                                <small id="emailHelp" class="form-text text-danger">Tenemos problemas con tu contraseña.</small>
                            </div>
                        </div>
                        <!-- <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div> -->
                        <button type="submit" class="btn btn-primary">Ingresar a itorank</button>
                    </form>
                    <a href="#" class="mt-5 d-block">: ’(  Si no tienes una cuenta en ITOrank, haz click aquí.</a>
                </div>
                <div class="col-sm-6 bg_ilustracion"></div>
            </div>
            
        </main>
        <!-- Bootstrap JS  -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous">
        </script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>

    </body>
</html>
