<?php include_once('inc/header.php'); ?>
    <body class="curriculum">
    <?php include_once('inc/navegacion.php'); ?>
        <main role="main" class="container">
            <div class="row home mt-2">
                <section class="central_cv col col-xl-7 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
                    
                    <div class="card card_curriculum">
                        <div class="card-header">Curriculm Vitae</div>
                        <div class="card-body">
                            <?php include_once('inc/cv/personal.php') ?>
                            
                            <?php include_once('inc/cv/experiencia.php') ?>
                            
                            <?php include_once('inc/cv/educacion.php') ?>
                            
                            <?php //include_once('inc/cv/recomendaciones.php') ?>

                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-primary">Guardar CV</button>
                        </div>
                    </div>

                </section>
                <aside class="aside_izq col col-xl-2 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-6 col-12">
                    
                    <?php //include_once('inc/aside_izq/info_personal.php') ?>

                    <?php //include_once('inc/aside_izq/keywords.php') ?>

                    <?php include_once('inc/aside_izq/performance.php') ?>

                    <?php include_once('inc/aside_izq/experiencia.php') ?>

                    <?php include_once('inc/aside_izq/conocimientos.php') ?>
                    
                </aside>
                <aside class="aside_der col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-6 col-12">
                    
                    <?php include_once('inc/aside_der/mas_profesionales.php') ?>
                    
                    <?php include_once('inc/aside_der/invitar.php') ?>
                    
                </aside>
            </div>
        </main>
        <!-- Bootstrap JS -->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1581152197/smartwizard/jquery.smartWizard.min.js"></script>
        <script src="js/stepper.jquery.js"></script>
        <!-- <script>
            $('.carousel').carousel()
        </script> -->
    </body>
</html>
