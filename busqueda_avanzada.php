<?php include_once('inc/header.php'); ?>
    <body class="home">
    <?php include_once('inc/navegacion.php'); ?>
        <main role="main" class="container">
            <div class="row home mt-2">
                <section class="central col col-xl-6 offset-xl-3 col-lg-12">
                    <h3 class="text-center mb-5 mt-4">Búsqueda de Profesionales</h3>
                    <form action="#">
                        <!-- Busqueda-->
                        <div class="mb-3">
                            <label for="nombre" class="sr-only">Nombres</label>
                            <input type="search" class="form-control" id="busqueda_profesionales" placeholder="Busca postulantes o profesionales" value="" required="true">
                            <div class="invalid-feedback">
                                Por favor ingrese una búsqueda válida.
                            </div>
                        </div>
                        <!-- segamento-->
                        <div class="mb-3">
                            <label for="nombre">Segmaneto</label>
                            <select name="segmento" id="segmento" class="form-control">
                                <option value="#">1</option>
                                <option value="#">2</option>
                                <option value="#">3</option>
                                <option value="#">4</option>
                                <option value="#">5</option>
                                <option value="#">6</option>
                            </select>
                            <div class="invalid-feedback">
                                Por favor ingrese una búsqueda válida.
                            </div>
                        </div>
                        <!-- metros -->
                        <p class="d-block">Metros<sup>2</sup></p>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                              <label for="metros_desde" class="sr-only">Metros<sup>2</sup> desde</label>
                              <input type="number" class="form-control" id="metros_desde">
                              <div class="invalid-feedback">
                                    Por favor ingrese un metraje válido.
                              </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="metros_hasta" class="sr-only">Metros<sup>2</sup> hasta</label>
                                <input type="number" class="form-control" id="metros_hasta">
                                <div class="invalid-feedback">
                                    Por favor ingrese un metraje válido.
                                </div>
                            </div>
                        </div>
                        <!-- slider -->
                        <div class="mb-3">
                            <label for="telefono">Años de experiencia</label>
                            <input type="range" list="tickmarks" min="5" max="30" class="range_list" style="width:100%;">
                            <datalist id="tickmarks" >
                                <option value="5" label="5">
                                <option value="6">
                                <option value="7">
                                <option value="8">
                                <option value="9">
                                <option value="10" label="10">
                                <option value="11">
                                <option value="12">
                                <option value="13">
                                <option value="14">
                                <option value="15" label="15">
                                <option value="16">
                                <option value="17">
                                <option value="18">
                                <option value="19">
                                <option value="20" label="20">
                                <option value="21">
                                <option value="22">
                                <option value="23">
                                <option value="24">
                                <option value="25" label="25">
                                <option value="26">
                                <option value="27">
                                <option value="28">
                                <option value="29">
                                <option value="30" label="30+">
                            </datalist>
                          <div class="invalid-feedback">
                            Please enter your shipping address.
                          </div>
                        </div>
                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Buscar</button>  
                    </form>

                </section>
            </div>
        </main>
        <!-- Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous">
        </script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
    </body>
</html>