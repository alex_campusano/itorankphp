<?php include_once('inc/header.php'); ?>
    <body class="home">
        <?php include_once('inc/navegacion.php'); ?>
        <main role="main" class="container">
            <div class="row home mt-2">
                <div class="col-12 col-md-8">
                    
                    <div class="card p-2 mb-3">
                        <div class="row">
                            <div class="col-4 col-md-3 col-lg-2">
                                <img src="img/user.jpg" alt="Nombre del usuario"  class="rounded-circle" width="100">
                            </div>
                            <div class="col-5 col-md-6 col-lg-8" >
                                <span class="d-block ml-3 label_12_ttu_fwb_gris"><i class="fas fa-map-marker-alt"></i> Las condes, RM</span>
                                <div class="ml-3 t21_rojo">Nombre del personaje</div>
                                <p class="ml-3 mb-0">Profesión</p>
                                <p class="ml-3 mt-0"><small>Escuela y universidad</small></p>
                            </div>
                            <div class="col-3 col-md-3 col-lg-2 text-right">
                                <a href="#" class="d-block mb-3">
                                    <span class="fa-stack fa-1x">
                                        <i class="fas fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-download fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                                <a href="#" class="d-block">
                                    <span class="fa-stack fa-1x">
                                        <i class="fas fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-envelope fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="perfil_acerca p-4">
                            <div class="titulo_perfil">Acerca de </div>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugiat, numquam incidunt illum omnis in consectetur quibusdam nulla et. Nisi accusamus soluta esse rem reprehenderit! Adipisci velit labore totam. Repudiandae, earum!</p>
                        </div>
                        <hr>
                        <div class="perfil_experiencia p-4">
                            <div class="titulo_perfil">Experiencia</div>
                            <!-- item experiencia -->
                            <div class="row experiencia mt-3 mb-5">
                                <div class="col-2">
                                    <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                </div>
                                <div class="col-10">
                                    <div class="cargo_trabajo">Cargo desempeñado</div>
                                    <span class="lugar_info_cv">Lugar de trabajo</span>
                                    <span class="fecha_info_cv">fecha de trabajo</span>
                                    <span class="direccion_info_cv">Direccion</spanp>
                                    <div class="label_13_ttu_fwb_rojo mb-2">Keywords</div>
                                    <div class="keywords_relacionados">
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Jefe de proyectos</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura de Proyectos</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Gestión de proyectos</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Sistemas</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura</a>
                                    </div>
                                </div>
                            </div>
                            <!-- item experiencia -->
                            <div class="row experiencia mt-3 mb-5">
                                <div class="col-2">
                                    <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                </div>
                                <div class="col-10">
                                    <div class="cargo_trabajo">Cargo desempeñado</div>
                                    <span class="lugar_info_cv">Lugar de trabajo</span>
                                    <span class="fecha_info_cv">fecha de trabajo</span>
                                    <span class="direccion_info_cv">Direccion</spanp>
                                    <div class="label_13_ttu_fwb_rojo mb-2">Keywords</div>
                                    <div class="keywords_relacionados">
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Jefe de proyectos</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura de Proyectos</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Gestión de proyectos</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Sistemas</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura</a>
                                    </div>
                                </div>
                            </div>
                            <!-- item experiencia -->
                            <div class="row experiencia mt-3 mb-5">
                                <div class="col-2">
                                    <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                </div>
                                <div class="col-10">
                                    <div class="cargo_trabajo">Cargo desempeñado</div>
                                    <span class="lugar_info_cv">Lugar de trabajo</span>
                                    <span class="fecha_info_cv">fecha de trabajo</span>
                                    <span class="direccion_info_cv">Direccion</spanp>
                                    <div class="label_13_ttu_fwb_rojo mb-2">Keywords</div>
                                    <div class="keywords_relacionados">
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Jefe de proyectos</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura de Proyectos</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Gestión de proyectos</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Sistemas</a>
                                        <a href="#" class="badge badge-pill badge-secondary mb-1">Jefatura</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr>
                        <div class="perfil_experiencia p-4">
                            <div class="titulo_perfil">Educacion</div>
                            <!-- item educacinoes -->
                            <div class="row experiencia mt-3 mb-5">
                                <div class="col-2">
                                    <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                </div>
                                <div class="col-10">
                                    <div class="titulo_educacion">Ingeniaería en algo</div>
                                    <span class="lugar_info_cv">Lugar de estudio</span>
                                    <span class="fecha_info_cv">fecha de estudio</span>
                                    <span class="escuela_educacion_cv">Escuela </span>
                                    <span class="direccion_info_cv">Direccion</span>
                                </div>
                            </div>
                            <!-- item educacinoes -->
                            <div class="row experiencia mt-3 mb-5">
                                <div class="col-2">
                                    <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                </div>
                                <div class="col-10">
                                    <div class="titulo_educacion">Ingeniaería en algo</div>
                                    <span class="lugar_info_cv">Lugar de estudio</span>
                                    <span class="fecha_info_cv">fecha de estudio</span>
                                    <span class="escuela_educacion_cv">Escuela </span>
                                    <span class="direccion_info_cv">Direccion</span>
                                </div>
                            </div>
                            <!-- item educacinoes -->
                            <div class="row experiencia mt-3 mb-5">
                                <div class="col-2">
                                    <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                </div>
                                <div class="col-10">
                                    <div class="titulo_educacion">Ingeniaería en algo</div>
                                    <span class="lugar_info_cv">Lugar de estudio</span>
                                    <span class="fecha_info_cv">fecha de estudio</span>
                                    <span class="escuela_educacion_cv">Escuela </span>
                                    <span class="direccion_info_cv">Direccion</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="perfil_experiencia p-4">
                            <div class="titulo_perfil">Recomendaciones</div>
                            <!-- item recomendacion -->
                            <div class="row experiencia mt-3 mb-5">
                                <div class="col-2">
                                    <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                </div>
                                <div class="col-10">
                                    <div class="titulo_educacion">Ingeniaería en algo</div>
                                    <span class="lugar_info_cv"> de donde </span>
                                    <span class="lugar_info_cv">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis, ullam alias sit iste inventore, fugiat dicta odit atque tenetur voluptates dolor commodi voluptate error ipsum, officia provident cupiditate eaque ipsa.</span>
                                </div>
                            </div>
                            <!-- item recomendacion -->
                            <div class="row experiencia mt-3 mb-5">
                                <div class="col-2">
                                    <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                </div>
                                <div class="col-10">
                                    <div class="titulo_educacion">Ingeniaería en algo</div>
                                    <span class="lugar_info_cv"> de donde </span>
                                    <span class="lugar_info_cv">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis, ullam alias sit iste inventore, fugiat dicta odit atque tenetur voluptates dolor commodi voluptate error ipsum, officia provident cupiditate eaque ipsa.</span>
                                </div>
                            </div>
                            <!-- item recomendacion -->
                            <div class="row experiencia mt-3 mb-5">
                                <div class="col-2">
                                    <img src="img/user_2.jpg" alt="" class="rounded-circle imagen_empresa">
                                </div>
                                <div class="col-10">
                                    <div class="titulo_educacion">Ingeniaería en algo</div>
                                    <span class="lugar_info_cv"> de donde </span>
                                    <span class="lugar_info_cv">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis, ullam alias sit iste inventore, fugiat dicta odit atque tenetur voluptates dolor commodi voluptate error ipsum, officia provident cupiditate eaque ipsa.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card estadisticas mb-3">
                        <div class="card-header">Estadisticas</div>
                        <div class="card-body">
                            <div class="col-12 ">
                                <div class="label_12_ttu_fwb_gris">Posición</div>
                                <p>10.500 aprox.</p>
                            </div>
                            <div class="col-12 ">
                                <div class="label_12_ttu_fwb_gris">metros <sup>2</sup></div>
                                <p>10.500 aprox.</p>
                            </div>
                            <hr class="hr_menor">
                            <div class="col-12 ">
                                <div class="label_12_ttu_fwb_gris">Años de Experienicia</div>
                                <p>10.500 aprox.</p>
                            </div>
                            <hr class="hr_menor">
                            <div class="col-12 ">
                                <div class="label_12_ttu_fwb_gris">Feedback Clientes</div>
                                <p>10.500 aprox.</p>
                            </div>
                            <div class="col-12 ">
                                <div class="label_12_ttu_fwb_gris">Feedback Jefaturas</div>
                                <p>10.500 aprox.</p>
                            </div>
                            <div class="col-12 ">
                                <div class="label_12_ttu_fwb_gris">Resultados Auditorias</div>
                                <p>10.500 aprox.</p>
                            </div>
                        </div>
                        
                    </div>
                    <?php include_once('inc/aside_izq/performance.php'); ?>
                    <?php include_once('inc/aside_izq/experiencia.php'); ?>
                    <?php include_once('inc/aside_izq/conocimientos.php'); ?>
                    <?php include_once('inc/aside_izq/keywords.php'); ?>
                </div>
            </div>
        </main>
        <!-- Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous">
        </script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.21/datatables.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#tabla_usuarios').DataTable();
            } );
        </script>
    </body>
</html>