<?php include_once('inc/header.php'); ?>
    <body class="home">
    <?php include_once('inc/navegacion.php'); ?>
        <main role="main" class="container">
            <div class="row home mt-2">
                <section class="central col col-xl-8 offset-xl-2 col-lg-12">
                    <form class="needs-validation" novalidate="">

                        <div class="mb-3">
                          <label for="nombre">Nombres</label>
                          <input type="text" class="form-control" id="nombre" placeholder="" value="" required="">
                          <div class="invalid-feedback">
                              Por favor ingrese un nombre válido.
                          </div>
                        </div>
                        <div class="mb-3">
                            <label for="apellido">Apellidos</label>
                            <input type="text" class="form-control" id="apellido" placeholder="" value="" required="">
                            <div class="invalid-feedback">
                                Por favor ingrese un apellido válido.
                            </div>
                        </div>


                        <div class="mb-3">
                            <label for="nombre_usuario">Nombre de usuario</label>
                            <input type="text" class="form-control" id="nombre_usuario" placeholder="">
                            <div class="invalid-feedback">
                                Por favor ingrese un nombre de usuario válido.
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                              <label for="passwords">Contraseña</label>
                              <input type="password" class="form-control" id="passwords">
                              <div class="invalid-feedback">
                                  La contraseña debe tener por lo menos 6 caracteres.
                              </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="apellido">Repita contraseña</label>
                                <input type="password" class="form-control" id="passwords">
                                <div class="invalid-feedback">
                                    La contraseña no coíncide.
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                          <label for="rut_usuario">Rut</label>
                          <input type="text" class="form-control" id="rut_usuario" placeholder="12345678-9" required="true">
                          <div class="invalid-feedback">
                              Por favor ingrese un rut válido.
                          </div>
                        </div>

                        <div class="mb-3">
                          <label for="fecha_nacimiento">Fecha de nacimiento</label>
                          <input type="date" class="form-control" id="fecha_nacimiento" placeholder="12345678-9" required="true">
                          <div class="invalid-feedback">
                              Por favor ingrese una fecha válida.
                          </div>
                        </div>

                        <div class="mb-3">
                          <label for="email">Email <span class="text-muted">(Optional)</span></label>
                          <input type="email" class="form-control" id="email" placeholder="you@example.com">
                          <div class="invalid-feedback">
                            Please enter a valid email address for shipping updates.
                          </div>
                        </div>

                        <div class="mb-3">
                          <label for="telefono">Teléfono</label>
                          <input type="tel" class="form-control" id="telefono" placeholder="" required="">
                          <div class="invalid-feedback">
                            Please enter your shipping address.
                          </div>
                        </div>						

                        <fieldset>
                            <legend>Sexo</legend>
                            <div class="d-inline my-3">
                              <div class="custom-control custom-radio">
                                <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked="" required="">
                                <label class="custom-control-label" for="credit">Masculino</label>
                              </div>
                              <div class="custom-control custom-radio">
                                <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required="">
                                <label class="custom-control-label" for="debit">Femenino</label>
                              </div>
                              <div class="custom-control custom-radio">
                                <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required="">
                                <label class="custom-control-label" for="paypal">Otro</label>
                              </div>
                            </div>
                        </fieldset>
                        <hr class="mb-4">
                        <div class="mb-3">
                          <label for="numeros">Números</label>
                          <input type="number" class="form-control" id="numeros" placeholder="" required="">
                          <div class="invalid-feedback">
                            Please enter your shipping address.
                          </div>
                        </div>
                        <div class="slidecontainer">

                        </div>
                        <hr class="mb-4">
                        <div class="mb-3">
                          <label for="telefono">Rango</label>
                          <input type="range" min="1" max="100" value="50" class="slider" id="myRange">
                          <div class="invalid-feedback">
                            Please enter your shipping address.
                          </div>
                        </div>

						<hr class="mb-4">

                        <fieldset>
                            <legend>Titulo del fieldset de checkboxes</legend>
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="same-address">
								<label class="custom-control-label" for="same-address">checkbox item</label>
							</div>
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="save-info">
								<label class="custom-control-label" for="save-info">Checkbox item</label>
							</div>
						</fielset>
                        <hr class="mb-4">

						<div class="mb-3">
                          <label for="telefono">Área de Texto</label>
                          <textarea name="area" id="areatexto"></textarea>
                          <div class="invalid-feedback">
                            Please enter your shipping address.
                          </div>
                        </div>

						<hr class="mb-4">

                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
                      </form>
                </section>
            </div>
        </main>
        <!-- Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous">
        </script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
    </body>
</html>
