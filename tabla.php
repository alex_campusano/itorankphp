<?php include_once('inc/header.php'); ?>
    <body class="home">
        <?php include_once('inc/navegacion.php'); ?>
        <main role="main" class="container">
            <div class="row home mt-2">
                <section class="central col col-xl-10 offset-xl-1 col-lg-12">
                    <h2>Usuarios</h2>
                    <!-- <div class="table-responsive"> -->
                        <table class="table table-striped " id="tabla_usuarios">
                            <thead>
                                <tr>
                                    <th scope="col">Nombre usuario</th>
                                    <th scope="col">Nombre completo</th>
                                    <th scope="col">Rut</th>
                                    <th scope="col">Teléfono</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Fecha Nac.</th>
                                    <th scope="col">acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Luke</td>
                                <td>Leigh Perez</td>
                                <td>8641124-5</td>
                                <td>(01) 0628 7648</td>
                                <td>Luke@gmai.com</td>
                                <td>26-09-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Lael</td>
                                <td>Shafira Melton</td>
                                <td>23300406-5</td>
                                <td>(01) 8451 8561</td>
                                <td>Lael@gmai.com</td>
                                <td>17-02-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Dalton</td>
                                <td>Jamalia Phillips</td>
                                <td>38437464-6</td>
                                <td>(09) 0385 9598</td>
                                <td>Dalton@gmai.com</td>
                                <td>29-06-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Xaviera</td>
                                <td>Isaiah Mendez</td>
                                <td>22947287-9</td>
                                <td>(03) 4878 4223</td>
                                <td>Xaviera@gmai.com</td>
                                <td>06-06-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Patience</td>
                                <td>Macy Orr</td>
                                <td>8402252-7</td>
                                <td>(02) 9609 3387</td>
                                <td>Patience@gmai.com</td>
                                <td>03-03-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Mona</td>
                                <td>Dana Gallagher</td>
                                <td>42566948-6</td>
                                <td>(03) 9162 3639</td>
                                <td>Mona@gmai.com</td>
                                <td>16-04-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Hamilton</td>
                                <td>Wynter Barrett</td>
                                <td>40994667-4</td>
                                <td>(07) 4687 9125</td>
                                <td>Hamilton@gmai.com</td>
                                <td>22-09-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Nero</td>
                                <td>Zelenia Blanchard</td>
                                <td>7932825-1</td>
                                <td>(07) 6405 2402</td>
                                <td>Nero@gmai.com</td>
                                <td>02-11-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Macey</td>
                                <td>Charissa Weeks</td>
                                <td>27759429-3</td>
                                <td>(06) 9884 6368</td>
                                <td>Macey@gmai.com</td>
                                <td>07-11-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Tallulah</td>
                                <td>Theodore Vargas</td>
                                <td>42968100-6</td>
                                <td>(05) 4447 9316</td>
                                <td>Tallulah@gmai.com</td>
                                <td>14-09-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Addison</td>
                                <td>Jillian Oneil</td>
                                <td>30027692-K</td>
                                <td>(08) 2124 8137</td>
                                <td>Addison@gmai.com</td>
                                <td>14-04-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Myra</td>
                                <td>Todd Lynch</td>
                                <td>26252897-9</td>
                                <td>(09) 3840 8836</td>
                                <td>Myra@gmai.com</td>
                                <td>06-03-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Brett</td>
                                <td>Allen Salas</td>
                                <td>36570366-3</td>
                                <td>(07) 1614 3821</td>
                                <td>Brett@gmai.com</td>
                                <td>27-11-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Yoshio</td>
                                <td>Dillon Mathews</td>
                                <td>7440221-6</td>
                                <td>(05) 0643 3802</td>
                                <td>Yoshio@gmai.com</td>
                                <td>22-03-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Galvin</td>
                                <td>Ramona Perkins</td>
                                <td>12795916-1</td>
                                <td>(04) 7873 5506</td>
                                <td>Galvin@gmai.com</td>
                                <td>21-02-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Yoko</td>
                                <td>Asher Barron</td>
                                <td>45451970-1</td>
                                <td>(05) 9272 7995</td>
                                <td>Yoko@gmai.com</td>
                                <td>20-10-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Cassandra</td>
                                <td>Nigel Lang</td>
                                <td>38422495-4</td>
                                <td>(02) 1292 4890</td>
                                <td>Cassandra@gmai.com</td>
                                <td>12-07-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Victor</td>
                                <td>Leonard Byrd</td>
                                <td>47179179-2</td>
                                <td>(07) 4360 0026</td>
                                <td>Victor@gmai.com</td>
                                <td>06-09-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Bertha</td>
                                <td>Velma Clarke</td>
                                <td>5960152-0</td>
                                <td>(09) 3769 4416</td>
                                <td>Bertha@gmai.com</td>
                                <td>24-05-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Blossom</td>
                                <td>Clayton Hardin</td>
                                <td>34476702-5</td>
                                <td>(05) 7145 4881</td>
                                <td>Blossom@gmai.com</td>
                                <td>19-06-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Hadassah</td>
                                <td>Oscar Avila</td>
                                <td>29162319-0</td>
                                <td>(02) 6577 7756</td>
                                <td>Hadassah@gmai.com</td>
                                <td>15-10-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Kellie</td>
                                <td>Gavin Combs</td>
                                <td>45016945-5</td>
                                <td>(09) 5398 6238</td>
                                <td>Kellie@gmai.com</td>
                                <td>23-07-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Shafira</td>
                                <td>Xaviera Oneal</td>
                                <td>13816400-4</td>
                                <td>(08) 6487 7221</td>
                                <td>Shafira@gmai.com</td>
                                <td>24-10-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Bert</td>
                                <td>Penelope Whitfield</td>
                                <td>9513712-1</td>
                                <td>(06) 9366 1226</td>
                                <td>Bert@gmai.com</td>
                                <td>03-09-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Gannon</td>
                                <td>Noelani Roberson</td>
                                <td>29354223-6</td>
                                <td>(08) 9737 5695</td>
                                <td>Gannon@gmai.com</td>
                                <td>22-08-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jenette</td>
                                <td>Lucy Vargas</td>
                                <td>13229165-9</td>
                                <td>(07) 3482 5609</td>
                                <td>Jenette@gmai.com</td>
                                <td>06-03-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Denton</td>
                                <td>Ursa Sims</td>
                                <td>37638563-9</td>
                                <td>(09) 5384 5608</td>
                                <td>Denton@gmai.com</td>
                                <td>13-11-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Palmer</td>
                                <td>Daquan Stewart</td>
                                <td>40259632-5</td>
                                <td>(05) 1325 5825</td>
                                <td>Palmer@gmai.com</td>
                                <td>01-12-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Yetta</td>
                                <td>Fay Collins</td>
                                <td>26114697-5</td>
                                <td>(07) 1511 6890</td>
                                <td>Yetta@gmai.com</td>
                                <td>07-06-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Howard</td>
                                <td>Serena Craig</td>
                                <td>38967159-2</td>
                                <td>(07) 7454 8769</td>
                                <td>Howard@gmai.com</td>
                                <td>01-05-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Bertha</td>
                                <td>Austin Hendricks</td>
                                <td>29116474-9</td>
                                <td>(07) 5028 9822</td>
                                <td>Bertha@gmai.com</td>
                                <td>04-03-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Aurora</td>
                                <td>Linda Velasquez</td>
                                <td>40403767-6</td>
                                <td>(06) 5536 9254</td>
                                <td>Aurora@gmai.com</td>
                                <td>28-02-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Shelly</td>
                                <td>Madonna Ayers</td>
                                <td>5712650-7</td>
                                <td>(01) 9622 4484</td>
                                <td>Shelly@gmai.com</td>
                                <td>24-07-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Indira</td>
                                <td>Kirestin Pacheco</td>
                                <td>23824856-6</td>
                                <td>(08) 8995 2600</td>
                                <td>Indira@gmai.com</td>
                                <td>27-12-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Garrison</td>
                                <td>Liberty Walls</td>
                                <td>33220092-5</td>
                                <td>(03) 9667 0731</td>
                                <td>Garrison@gmai.com</td>
                                <td>30-08-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Catherine</td>
                                <td>Edan Baxter</td>
                                <td>10428951-7</td>
                                <td>(07) 4260 2326</td>
                                <td>Catherine@gmai.com</td>
                                <td>11-11-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Zenia</td>
                                <td>Gretchen Pace</td>
                                <td>22815731-7</td>
                                <td>(01) 1194 3524</td>
                                <td>Zenia@gmai.com</td>
                                <td>02-02-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Quincy</td>
                                <td>Janna Sheppard</td>
                                <td>8864152-3</td>
                                <td>(07) 3978 8576</td>
                                <td>Quincy@gmai.com</td>
                                <td>28-03-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Ulla</td>
                                <td>Sylvester Ryan</td>
                                <td>27421048-6</td>
                                <td>(08) 8581 3330</td>
                                <td>Ulla@gmai.com</td>
                                <td>03-07-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Charlotte</td>
                                <td>Lamar Sanders</td>
                                <td>28540235-2</td>
                                <td>(04) 5638 0555</td>
                                <td>Charlotte@gmai.com</td>
                                <td>30-07-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Aladdin</td>
                                <td>Halla Wilder</td>
                                <td>21351450-4</td>
                                <td>(05) 2598 0394</td>
                                <td>Aladdin@gmai.com</td>
                                <td>27-12-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Allen</td>
                                <td>Benedict Noble</td>
                                <td>46937773-3</td>
                                <td>(09) 2056 6748</td>
                                <td>Allen@gmai.com</td>
                                <td>07-03-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jerry</td>
                                <td>Boris Gamble</td>
                                <td>49096354-5</td>
                                <td>(01) 9734 2685</td>
                                <td>Jerry@gmai.com</td>
                                <td>14-11-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Sonya</td>
                                <td>Maisie Henry</td>
                                <td>12964338-2</td>
                                <td>(04) 6803 2422</td>
                                <td>Sonya@gmai.com</td>
                                <td>09-11-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Logan</td>
                                <td>Jessica Roach</td>
                                <td>26728394-K</td>
                                <td>(01) 4870 2632</td>
                                <td>Logan@gmai.com</td>
                                <td>18-06-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Kimberly</td>
                                <td>Leigh Harrison</td>
                                <td>11712258-1</td>
                                <td>(01) 8347 8207</td>
                                <td>Kimberly@gmai.com</td>
                                <td>08-02-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Bryar</td>
                                <td>Nero Martin</td>
                                <td>45040069-6</td>
                                <td>(05) 3393 0786</td>
                                <td>Bryar@gmai.com</td>
                                <td>22-06-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Nasim</td>
                                <td>Ferris Thomas</td>
                                <td>31716566-8</td>
                                <td>(05) 1619 4409</td>
                                <td>Nasim@gmai.com</td>
                                <td>14-01-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Lars</td>
                                <td>Rhonda Franco</td>
                                <td>8373594-5</td>
                                <td>(05) 7059 6157</td>
                                <td>Lars@gmai.com</td>
                                <td>23-05-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Kelsey</td>
                                <td>Erica Casey</td>
                                <td>32563856-7</td>
                                <td>(09) 8198 2341</td>
                                <td>Kelsey@gmai.com</td>
                                <td>19-05-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Caldwell</td>
                                <td>Emma Coleman</td>
                                <td>47697674-K</td>
                                <td>(04) 5741 4269</td>
                                <td>Caldwell@gmai.com</td>
                                <td>30-08-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>   
                            <tr>
                                <td>Brady</td>
                                <td>Desirae Miranda</td>
                                <td>6876732-6</td>
                                <td>(07) 0114 1576</td>
                                <td>Brady@gmai.com</td>
                                <td>05-06-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Ina</td>
                                <td>Tobias Lawson</td>
                                <td>27075506-2</td>
                                <td>(09) 8935 1456</td>
                                <td>Ina@gmai.com</td>
                                <td>28-08-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Josephine</td>
                                <td>Lisandra Burke</td>
                                <td>36786903-8</td>
                                <td>(01) 9872 4622</td>
                                <td>Josephine@gmai.com</td>
                                <td>23-08-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Faith</td>
                                <td>William Mcguire</td>
                                <td>6641495-7</td>
                                <td>(06) 6494 2989</td>
                                <td>Faith@gmai.com</td>
                                <td>04-06-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Dakota</td>
                                <td>Anjolie Shelton</td>
                                <td>34551873-8</td>
                                <td>(05) 1102 0410</td>
                                <td>Dakota@gmai.com</td>
                                <td>11-12-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Reese</td>
                                <td>Kareem Mcfadden</td>
                                <td>13652111-K</td>
                                <td>(04) 2472 7979</td>
                                <td>Reese@gmai.com</td>
                                <td>09-10-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Amity</td>
                                <td>Evangeline Sykes</td>
                                <td>8094153-6</td>
                                <td>(07) 5760 6835</td>
                                <td>Amity@gmai.com</td>
                                <td>05-07-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Roth</td>
                                <td>Tamekah Jimenez</td>
                                <td>18740335-9</td>
                                <td>(03) 7332 2243</td>
                                <td>Roth@gmai.com</td>
                                <td>02-11-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Karen</td>
                                <td>Halee Lancaster</td>
                                <td>30908950-2</td>
                                <td>(04) 1488 1926</td>
                                <td>Karen@gmai.com</td>
                                <td>30-10-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Zephr</td>
                                <td>Brenda Roberts</td>
                                <td>27736040-3</td>
                                <td>(05) 3301 1180</td>
                                <td>Zephr@gmai.com</td>
                                <td>19-11-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Harrison</td>
                                <td>Hilary Holcomb</td>
                                <td>7434587-5</td>
                                <td>(02) 9057 4093</td>
                                <td>Harrison@gmai.com</td>
                                <td>11-05-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Carson</td>
                                <td>Ezekiel Wiley</td>
                                <td>33268971-1</td>
                                <td>(05) 3732 0708</td>
                                <td>Carson@gmai.com</td>
                                <td>30-10-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Tatum</td>
                                <td>Chester Cabrera</td>
                                <td>15263793-4</td>
                                <td>(03) 2019 7241</td>
                                <td>Tatum@gmai.com</td>
                                <td>28-08-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Shana</td>
                                <td>Uta Ashley</td>
                                <td>48386179-6</td>
                                <td>(03) 2397 7414</td>
                                <td>Shana@gmai.com</td>
                                <td>24-05-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Beverly</td>
                                <td>Amena Alexander</td>
                                <td>6154840-8</td>
                                <td>(05) 3763 6522</td>
                                <td>Beverly@gmai.com</td>
                                <td>18-10-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Allen</td>
                                <td>Neville Gregory</td>
                                <td>27060718-7</td>
                                <td>(03) 3526 5501</td>
                                <td>Allen@gmai.com</td>
                                <td>12-07-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Fulton</td>
                                <td>Judith Shelton</td>
                                <td>16903041-3</td>
                                <td>(01) 1896 3846</td>
                                <td>Fulton@gmai.com</td>
                                <td>15-02-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Gay</td>
                                <td>Doris Flores</td>
                                <td>15500206-9</td>
                                <td>(02) 1136 7126</td>
                                <td>Gay@gmai.com</td>
                                <td>29-11-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Xanthus</td>
                                <td>Keegan Whitley</td>
                                <td>6137007-2</td>
                                <td>(04) 4143 6281</td>
                                <td>Xanthus@gmai.com</td>
                                <td>21-06-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Rae</td>
                                <td>Kelly Cochran</td>
                                <td>24675514-0</td>
                                <td>(01) 7051 5082</td>
                                <td>Rae@gmai.com</td>
                                <td>24-01-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Scarlet</td>
                                <td>Karen Hood</td>
                                <td>38374422-9</td>
                                <td>(05) 1878 9485</td>
                                <td>Scarlet@gmai.com</td>
                                <td>26-06-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Stephanie</td>
                                <td>Serina Perez</td>
                                <td>27389021-1</td>
                                <td>(03) 1184 7360</td>
                                <td>Stephanie@gmai.com</td>
                                <td>26-11-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Abraham</td>
                                <td>Davis Briggs</td>
                                <td>33679963-5</td>
                                <td>(01) 3410 9879</td>
                                <td>Abraham@gmai.com</td>
                                <td>10-02-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Jocelyn</td>
                                <td>Seth Little</td>
                                <td>9248867-5</td>
                                <td>(09) 0274 0303</td>
                                <td>Jocelyn@gmai.com</td>
                                <td>21-11-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Shelly</td>
                                <td>Shelly Riley</td>
                                <td>47854006-K</td>
                                <td>(03) 8794 4368</td>
                                <td>Shelly@gmai.com</td>
                                <td>27-01-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Cooper</td>
                                <td>Karleigh Marks</td>
                                <td>44392372-1</td>
                                <td>(04) 9335 8724</td>
                                <td>Cooper@gmai.com</td>
                                <td>07-09-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Tarik</td>
                                <td>Honorato Vazquez</td>
                                <td>11266293-6</td>
                                <td>(06) 0090 2475</td>
                                <td>Tarik@gmai.com</td>
                                <td>14-01-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Vivian</td>
                                <td>Cara Johnson</td>
                                <td>19330209-2</td>
                                <td>(08) 2714 6452</td>
                                <td>Vivian@gmai.com</td>
                                <td>09-07-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Maggie</td>
                                <td>Steven Andrews</td>
                                <td>49416282-2</td>
                                <td>(04) 2674 4001</td>
                                <td>Maggie@gmai.com</td>
                                <td>27-01-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Hoyt</td>
                                <td>Holmes Munoz</td>
                                <td>12321939-2</td>
                                <td>(02) 1871 3242</td>
                                <td>Hoyt@gmai.com</td>
                                <td>01-08-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Kennan</td>
                                <td>Molly Moses</td>
                                <td>34000262-8</td>
                                <td>(04) 9832 1151</td>
                                <td>Kennan@gmai.com</td>
                                <td>02-04-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>George</td>
                                <td>Joy Guerrero</td>
                                <td>11030515-K</td>
                                <td>(02) 9554 9700</td>
                                <td>George@gmai.com</td>
                                <td>13-10-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Darrel</td>
                                <td>Anthony Maxwell</td>
                                <td>9992900-6</td>
                                <td>(05) 6236 0188</td>
                                <td>Darrel@gmai.com</td>
                                <td>21-10-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Lars</td>
                                <td>Quinlan Camacho</td>
                                <td>47276237-0</td>
                                <td>(04) 4052 9655</td>
                                <td>Lars@gmai.com</td>
                                <td>15-08-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Yoshio</td>
                                <td>Lunea Clarke</td>
                                <td>28516637-3</td>
                                <td>(01) 3070 4111</td>
                                <td>Yoshio@gmai.com</td>
                                <td>17-09-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Cherokee</td>
                                <td>Garrison Morgan</td>
                                <td>38094190-2</td>
                                <td>(07) 7112 3022</td>
                                <td>Cherokee@gmai.com</td>
                                <td>16-11-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Cody</td>
                                <td>Maryam Deleon</td>
                                <td>31249803-0</td>
                                <td>(05) 1218 0835</td>
                                <td>Cody@gmai.com</td>
                                <td>10-05-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Axel</td>
                                <td>Selma Aguirre</td>
                                <td>5514878-3</td>
                                <td>(06) 1111 2028</td>
                                <td>Axel@gmai.com</td>
                                <td>06-11-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Karyn</td>
                                <td>Leandra Curry</td>
                                <td>24286361-5</td>
                                <td>(06) 4250 1310</td>
                                <td>Karyn@gmai.com</td>
                                <td>21-05-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Angelica</td>
                                <td>Caryn Conner</td>
                                <td>30726435-8</td>
                                <td>(04) 1811 8444</td>
                                <td>Angelica@gmai.com</td>
                                <td>21-11-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Cheryl</td>
                                <td>Lee Ingram</td>
                                <td>34366887-2</td>
                                <td>(08) 3359 9172</td>
                                <td>Cheryl@gmai.com</td>
                                <td>07-04-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Chloe</td>
                                <td>Blaine Franks</td>
                                <td>45257711-9</td>
                                <td>(09) 8875 9734</td>
                                <td>Chloe@gmai.com</td>
                                <td>08-03-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Hayes</td>
                                <td>Hyacinth Wilkerson</td>
                                <td>8746697-3</td>
                                <td>(01) 7905 3991</td>
                                <td>Hayes@gmai.com</td>
                                <td>05-06-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Drew</td>
                                <td>Eden Mcgowan</td>
                                <td>38314043-9</td>
                                <td>(05) 4315 6992</td>
                                <td>Drew@gmai.com</td>
                                <td>01-09-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Megan</td>
                                <td>Garth Suarez</td>
                                <td>21640430-0</td>
                                <td>(02) 6580 1007</td>
                                <td>Megan@gmai.com</td>
                                <td>17-07-19</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Vladimir</td>
                                <td>Hanna Thornton</td>
                                <td>22759923-5</td>
                                <td>(05) 0465 3853</td>
                                <td>Vladimir@gmai.com</td>
                                <td>17-05-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Erica</td>
                                <td>Ignacia Bruce</td>
                                <td>32664999-6</td>
                                <td>(07) 8461 3008</td>
                                <td>Erica@gmai.com</td>
                                <td>04-04-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Kennedy</td>
                                <td>Grady Leblanc</td>
                                <td>48455305-K</td>
                                <td>(02) 2516 4550</td>
                                <td>Kennedy@gmai.com</td>
                                <td>07-05-20</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Castor</td>
                                <td>Brenden Livingston</td>
                                <td>16769392-K</td>
                                <td>(09) 2217 0984</td>
                                <td>Castor@gmai.com</td>
                                <td>23-01-21</td>
                                <td>
                                    <a href="#">Op1</a>
                                    <a href="#">Op2</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    <!-- </div> -->
                </section>
            </div>
        </main>
        <!-- Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous">
        </script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.21/datatables.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#tabla_usuarios').DataTable();
            } );
        </script>
    </body>
</html>
